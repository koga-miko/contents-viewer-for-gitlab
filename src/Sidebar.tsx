import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import {
  Box,
  Drawer,
  IconButton,
  List,
  ListItemButton,
  Typography,
} from '@mui/material';

const Sidebar = () => {
  const location = useLocation();
  // eslint-disable-next-line no-console
  console.log(`location.pathname:${location.pathname}`);
  return (
    <Drawer
      container={window.document.body}
      variant="permanent"
      open
      sx={{ width: 200, height: '100vh' }}
    >
      <List
        sx={{
          width: 200,
          height: '100vh',
          backgroundColor: '#f7f7f7',
        }}
      >
        <ListItemButton
          component={Link}
          to="/Home"
          selected={location.pathname === '/' || location.pathname === '/Home'}
        >
          <Typography variant="body2" fontWeight="700">
            Home
          </Typography>
          <IconButton />
        </ListItemButton>
        <Box sx={{ paddingTop: '10px' }} />
        <ListItemButton
          component={Link}
          to="/Settings"
          selected={location.pathname === '/Settings'}
        >
          <Typography variant="body2" fontWeight="700">
            Settings
          </Typography>
          <IconButton />
        </ListItemButton>
      </List>
    </Drawer>
  );
};

export default Sidebar;
