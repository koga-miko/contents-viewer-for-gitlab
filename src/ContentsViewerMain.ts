import { exec } from "child_process";
import { simpleGit } from "simple-git";
import { app, /*shell,*/ ipcMain, dialog } from "electron";
import Store from "electron-store";
import { promises as fs } from "fs";
import path from "path";

const store = new Store();
export default class ContentsViewerMain {
  // IPCチャンネル名
  static CONTENTS_VIEWER_CHANNEL = "contents-viewer-channel";

  // ユーザー名
  private userName: string;

  // パスワード
  private password: string;

  // プロキシ使用するかどうか(true/false)
  private proxyUsed: boolean;

  // プロキシパス名（例：hostname/ポート番号）
  private proxyName: string;

  // 最初に起動するhtmlの相対パス
  private entryHtmlPath: string;

  // 作業ディレクトリ
  private workDir: string;

  // デフォルトの起動htmlpath
  private static DEFAULT_HTML_PATH = "public/index.html";

  // デフォルトの作業ディレクトリ
  private static DEFAULT_WORK_DIR = path.join(
    app.getPath("userData"),
    "contentsWork"
  );

  constructor(
    userName: string,
    password: string,
    proxyUsed: boolean,
    proxyName: string,
    entryHtmlPath: string,
    workdir: string
  ) {
    this.userName = userName;
    this.password = password;
    this.proxyUsed = proxyUsed;
    this.proxyName = proxyName;
    this.entryHtmlPath = entryHtmlPath;
    this.workDir = workdir;
  }

  static async ipcMainHandle() {
    ipcMain.handle(
      ContentsViewerMain.CONTENTS_VIEWER_CHANNEL,
      async (event, arg) => {
        return ContentsViewerMain.handleIpc(event, arg);
      }
    );
  }

  static async handleIpc(event: any, arg: any) {
    let retVal: unknown = undefined;
    if (arg[0] === "set") {
      store.set(arg[1], arg[2]);
      retVal = true;
    } else if (arg[0] === "get") {
      retVal = store.get(arg[1]);
      // 値によってケア必要なものだけケアする
      if (arg[1] === "entryHtmlPath" && !retVal) {
        // 起動HTMLパスが空ならデフォルトパスで丸める
        retVal = ContentsViewerMain.DEFAULT_HTML_PATH;
        store.set(arg[1], retVal);
      } else if (arg[1] === "workDir" && !retVal) {
        // 作業ディレクトリが空ならデフォルトパスで丸める
        retVal = ContentsViewerMain.DEFAULT_WORK_DIR;
        store.set(arg[1], retVal);
      } else {
        // ケア不要な値のため何もしない(正常系)
      }
    } else if (arg[0] === "action") {
      if (arg[1] === "showContents") {
        console.log(`Action: ${arg[1]}`);
        const userNameTmp = store.get("userName");
        const userName: string =
          typeof userNameTmp === "string" ? userNameTmp : "";

        const passwordTmp = store.get("password");
        const password: string =
          typeof passwordTmp === "string" ? passwordTmp : "";

        const proxyUsedTmp = store.get("proxyUsed");
        const proxyUsed: boolean =
          typeof proxyUsedTmp === "boolean" ? proxyUsedTmp : false;

        const proxyNameTmp = store.get("proxyName");
        const proxyName: string =
          typeof proxyNameTmp === "string" ? proxyNameTmp : "";

        const entryHtmlPathTmp = store.get("entryHtmlPath");
        const entryHtmlPath: string =
          typeof entryHtmlPathTmp === "string"
            ? entryHtmlPathTmp
            : ContentsViewerMain.DEFAULT_HTML_PATH;

        const urlTmp = store.get("url");
        const url: string = typeof urlTmp === "string" ? urlTmp : "";

        const workDirTmp = store.get("workDir");
        const workDir: string =
          typeof workDirTmp === "string"
            ? workDirTmp
            : ContentsViewerMain.DEFAULT_WORK_DIR;

        retVal = new ContentsViewerMain(
          userName,
          password,
          proxyUsed,
          proxyName,
          entryHtmlPath,
          workDir
        ).showContents(url);
      } else if (arg[1] === "showOpenDialogForDirectory") {
        retVal = ContentsViewerMain.showOpenDialogForDirectory();
      } else {
        console.log(`invalid value:${arg[1]}`);
      }
    } else {
      console.log(`invalid method:${arg[0]}`);
    }
    return retVal;
  }

  static showOpenDialogForDirectory() {
    return new Promise((resolve, reject) => {
      (async () => {
        let workDir = ContentsViewerMain.DEFAULT_WORK_DIR;
        const workDirTmp = store.get("workDir");
        if (typeof workDirTmp === "string") {
          try {
            await fs.access(workDirTmp);
            // アクセスできた（＝例外へ飛ばなかった）のでworkDirを更新する
            workDir = workDirTmp;
          } catch (e) {
            // アクセスできなかったらworkDirは更新しないがworkDirが存在しない場合はフォルダを作成する
            try {
              await fs.access(workDir);
            } catch (e) {
              try {
                await fs.mkdir(workDir);
              } catch (e) {
                // ここ二もし来てしまった場合は継続（ダイアログのフェールセーフ処理にゆだねる）
              }
            }
          }
        }
        dialog
          .showOpenDialog({
            title: "作業フォルダを選択してください",
            defaultPath: workDir,
            properties: ["openDirectory", "dontAddToRecent"],
          })
          .then((obj) => {
            if (obj.filePaths[0]) {
              resolve(obj.filePaths[0]);
            } else {
              reject(new Error("作業フォルダは選択されませんでした"));
            }
          })
          .catch(() => {
            reject(new Error("作業フォルダは選択されませんでした"));
          });
      })();
    });
  }

  showContents(url: string) {
    return new Promise((resolve, reject) => {
      (async () => {
        let targetDir = "";

        // マージリクエストのページかどうかをチェック
        // (リポジトリ名の直前のスラッシュまでの文字列)(リポジトリ名の文字列)/-/tree/(ブランチ名)
        let m = url.match(/(^.+)\/([^/]+)\/-\/tree\/([^/]+)\/?$/);
        // 上記形式ではなかった場合
        if (!m) {
          // (リポジトリ名の直前のスラッシュまでの文字列)(リポジトリ名の文字列) ※リポジトリのトップのURLとして判断
          m = url.match(/(^.+)\/([^/]+)\/?$/);
          if (!m) {
            reject(
              new Error(
                `パスが想定外のためリポジトリ名／ブランチ名を取得できませんでした{url: ${url}}`
              )
            );
            return;
          } else {
            // リポジトリのトップの場合はブランチ名の部分は抽出できていないので空文字を設定
            m[3] = "";
          }
        }
        try {
          targetDir = await this.getGitBranchContents(
            this.workDir, // 作業ディレクトリ
            `${m[1]}/${m[2]}`, // リポジトリのフルパス
            m[2], // リポジトリ名
            m[3] // ブランチ名
          );
        } catch (e) {
          reject(e);
          return;
        }
        const contentsUrl = `${targetDir}/${this.entryHtmlPath}`;

        // 最初に起動するHTMLファイルの有無をチェック。もしファイルが無い場合は例外をcatchしてエラー修了。
        try {
          await fs.access(contentsUrl);
        } catch (e) {
          reject(
            new Error(
              `最初に起動するHTMLファイル ${contentsUrl} が見つかりませんでした。`
            )
          );
          return;
        }

        // コンテンツを標準ブラウザで開く
        // try {
        //   console.log(`contentsUrl:${contentsUrl}`);
        //   await shell.openExternal(`file://${contentsUrl}`);
        // } catch (e) {
        //   console.log(`e2:${e}`);
        //   reject(new Error(`コンテンツ表示に失敗しました{url: ${contentsUrl}`));
        //   return;
        // }

        // 上記shell.openExternalでは日本語文字の時にエラー発生するためexecを使う形へ変更した
        exec(
          `rundll32.exe url.dll,FileProtocolHandler ${contentsUrl}`,
          (error, stdout, stderr) => {
            if (error) {
              console.log(`exec error: ${error}`);
            }
          }
        );
        resolve(true);
      })();
    });
  }

  // eslint-disable-next-line class-methods-use-this
  private getGitBranchContents(
    workDir: string,
    repoPath: string,
    repoName: string,
    branchName: string
  ) {
    const targetDir = path.join(
      workDir,
      branchName ? `${repoName}__${branchName}` : `${repoName}__@TOP@`
    );
    return new Promise<string>((resolve, reject) => {
      (async () => {
        // 作業用ディレクトリの作成
        try {
          await fs.access(workDir);
        } catch (e) {
          try {
            await fs.mkdir(workDir);
          } catch (e) {
            reject(
              new Error(`フォルダ作成に失敗しました {workDir: ${workDir}}`)
            );
            return;
          }
        }

        // （初回のみ）git cloneの実施
        try {
          await fs.access(targetDir);
        } catch (e) {
          // ※targetDirにアクセスできない（＝フォルダが無い）ためにこのcatchへ入った時を初回と判断してgit cloneを実施
          try {
            await simpleGit().clone(repoPath, targetDir);
          } catch (e) {
            reject(
              new Error(`git clone に失敗しました {repoPath: ${repoPath}}`)
            );
            return;
          }
        }

        try {
          const git = simpleGit(targetDir);

          // 該当ブランチのフェッチ
          await git.fetch("origin");

          // ブランチ名が指定されているときは該当ブランチをチェックアウトする
          if (branchName) {
            const branches = await git.branch();
            // まだローカルにブランチが存在しない場合はブランチ作成とチェックアウトを同時におこなう
            if (branches.all.indexOf(branchName) < 0) {
              await git.checkoutBranch(branchName, `origin/${branchName}`);
            } else {
              await git.checkout(branchName);
            }
          }
        } catch (e) {
          reject(
            new Error(`チェックアウトに失敗しました{branchName: ${branchName}}`)
          );
          return;
        }
        resolve(targetDir);
      })();
    });
  }
}
