import { Box, TextField } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import React, { useState, useEffect } from "react";
import ContentsViwerRenderer from "./ContentsViewerRenderer";

const Home = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [urlErrText, setUrlErrTxt] = useState<string>("");
  const [url, setUrl] = useState<string>("");

  useEffect(() => {
    ContentsViwerRenderer.getUrl()
      .then((value) => {
        if (typeof value === "string") {
          setUrl(value);
        } else {
          setUrl("");
        }
        return value;
      })
      .catch((e) => console.log(e));
  }, []);

  const hundleUrlChange = (e: any) => {
    setUrl(e.target.value);
    ContentsViwerRenderer.setUrl(e.target.value);
    if (url !== "") {
      setUrlErrTxt("");
    }
  };
  const hundleSubmit = (e: any) => {
    e.preventDefault();

    setUrlErrTxt("");

    // 入力欄の文字列を取得
    const data = new FormData(e.target);
    const tmpUrl = data.get("url");
    setUrl(tmpUrl !== null ? tmpUrl.toString().trim() : "");

    let error = false;

    if (url === "") {
      error = true;
      setUrlErrTxt(
        "表示したいコンテンツが含まれるGitlabのURLを入力してください"
      );
    }

    if (error) return;

    setLoading(true);
    ContentsViwerRenderer.showContents()
      .then((value) => {
        console.log("Success");
        setLoading(false);
        return value;
      })
      .catch((err) => {
        setLoading(false);
        error = true;
        setUrlErrTxt(err.message);
      });
  };

  return (
    <>
      <Box component="form" onSubmit={hundleSubmit} noValidate>
        <TextField
          fullWidth
          id="url"
          value={url}
          onChange={hundleUrlChange}
          label="Gitlab URL"
          margin="normal"
          name="url"
          required
          helperText={urlErrText}
          error={urlErrText !== ""}
          disabled={loading}
        />
        <LoadingButton
          sx={{ mt: 3, mb: 2 }}
          fullWidth
          type="submit"
          loading={loading}
          variant="outlined"
        >
          コンテンツを表示
        </LoadingButton>
      </Box>
    </>
  );
};

export default Home;
