import { contextBridge, ipcRenderer } from "electron";

contextBridge.exposeInMainWorld("electron", {
  ipcRenderer: {
    invoke(channel: string, args: unknown[]): Promise<unknown> {
      return ipcRenderer.invoke(channel, args);
    },
  },
});
