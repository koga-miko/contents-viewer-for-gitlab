import {
  Box,
  TextField,
  Checkbox,
  FormControlLabel,
  IconButton,
  Button,
} from "@mui/material";
import React, { useState, useEffect } from "react";
import FolderOpenOutlinedIcon from "@mui/icons-material/FolderOpenOutlined";
import ContentsViwerRenderer from "./ContentsViewerRenderer";

const Settings = () => {
  const [userName, setUserName] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [proxyUsed, setProxyUsed] = useState<boolean>(false);
  const [proxyName, setProxyName] = useState<string>("");
  const [entryHtmlPath, setEntryHtmlPath] = useState<string>("");
  const [workDir, setWorkDir] = useState<string>("");

  useEffect(() => {
    ContentsViwerRenderer.getUserName()
      .then((value) => {
        if (typeof value === "string") {
          setUserName(value);
        } else {
          setUserName("");
        }
        return value;
      })
      .catch((e) => console.log(e));
    ContentsViwerRenderer.getPassword()
      .then((value) => {
        if (typeof value === "string") {
          setPassword(value);
        } else {
          setPassword("");
        }
        return value;
      })
      .catch((e) => console.log(e));
    ContentsViwerRenderer.getProxyUsed()
      .then((value) => {
        if (typeof value === "boolean") {
          setProxyUsed(value);
        } else {
          setProxyUsed(false);
        }
        return value;
      })
      .catch((e) => console.log(e));
    ContentsViwerRenderer.getProxyName()
      .then((value) => {
        if (typeof value === "string") {
          setProxyName(value);
        } else {
          setProxyName("");
        }
        return value;
      })
      .catch((e) => console.log(e));
    ContentsViwerRenderer.getEntryHtmlPath()
      .then((value) => {
        if (typeof value === "string") {
          setEntryHtmlPath(value);
        } else {
          setEntryHtmlPath("");
        }
        return value;
      })
      .catch((e) => console.log(e));
    ContentsViwerRenderer.getWorkDir()
      .then((value) => {
        if (typeof value === "string") {
          setWorkDir(value);
        } else {
          setWorkDir("");
        }
        return value;
      })
      .catch((e) => console.log(e));
  }, []);
  const hundleUserName = (e: any) => {
    setUserName(e.target.value);
    ContentsViwerRenderer.setUserName(e.target.value);
  };
  const hundlePassword = (e: any) => {
    setPassword(e.target.value);
    ContentsViwerRenderer.setPassword(e.target.value);
  };
  const handleProxyChange = (e: any) => {
    const newValue = !proxyUsed;
    setProxyUsed(newValue);
    ContentsViwerRenderer.setProxyUsed(newValue);
  };
  const hundleProxyName = (e: any) => {
    setProxyName(e.target.value);
    ContentsViwerRenderer.setProxyName(e.target.value);
  };
  const hundleEntryHtmlPath = (e: any) => {
    setEntryHtmlPath(e.target.value);
    ContentsViwerRenderer.setEntryHtmlPath(e.target.value);
  };
  const hundleWorkDir = (e: any) => {
    setWorkDir(e.target.value);
    ContentsViwerRenderer.setWorkDir(e.target.value);
  };
  const hundleOpenFolder = () => {
    ContentsViwerRenderer.showOpenDialogForDirectory().then((dirPath) => {
      if (typeof dirPath === "string" && dirPath) {
        setWorkDir(dirPath);
        ContentsViwerRenderer.setWorkDir(dirPath);
      }
    });
  };
  return (
    <>
      <TextField
        fullWidth
        id="entryHtmlPath"
        label="最初に起動するHTMLファイル相対パス"
        margin="normal"
        name="entryHtmlPath"
        value={entryHtmlPath}
        onChange={hundleEntryHtmlPath}
      />
      <Box sx={{ paddingTop: "20px" }} />
      <Box sx={{ display: "flex" }}>
        <TextField
          fullWidth
          id="workDir"
          label="コンテンツをチェックアウトするフォルダ"
          margin="normal"
          name="workDir"
          value={workDir}
          onChange={hundleWorkDir}
        />
        <Button
          variant="outlined"
          sx={{ marginTop: 2, marginBottom: 1 }}
          onClick={() => {
            hundleOpenFolder();
          }}
        >
          <FolderOpenOutlinedIcon></FolderOpenOutlinedIcon>
        </Button>
      </Box>
      {/* <Box sx={{ paddingTop: "20px" }} />
      <TextField
        fullWidth
        id="username"
        label="Gitlab ユーザー名"
        margin="normal"
        name="username"
        required
        value={userName}
        onChange={hundleUserName}
      />
      <TextField
        fullWidth
        id="password"
        label="Gitlab パスワード"
        margin="normal"
        name="password"
        type="password"
        required
        value={password}
        onChange={hundlePassword}
      />
      <Box sx={{ paddingTop: "20px" }} />
      <FormControlLabel
        control={<Checkbox checked={proxyUsed} onChange={handleProxyChange} />}
        label="プロキシを使用する"
      />
      <TextField
        fullWidth
        id="proxyName"
        label="プロキシ名（例：hostname:port）"
        margin="normal"
        name="proxyName"
        value={proxyName}
        // helperText={passwordErrText}
        // error={passwordErrText !== ''}
        disabled={!proxyUsed}
        onChange={hundleProxyName}
      /> */}
    </>
  );
};

export default Settings;
