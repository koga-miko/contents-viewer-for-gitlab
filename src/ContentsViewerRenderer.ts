const CONTENTS_VIEWER_CHANNEL = "contents-viewer-channel";

declare global {
  interface Window {
    electron: {
      ipcRenderer: {
        invoke(channel: string, args: unknown[]): Promise<unknown>;
      };
    };
  }
}

const ContentsViwerRenderer = {
  setUrl: (url: string) => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "set",
      "url",
      url,
    ]);
  },
  setUserName: (userName: string) => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "set",
      "userName",
      userName,
    ]);
  },
  setPassword: (password: string) => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "set",
      "password",
      password,
    ]);
  },
  setProxyUsed: (proxyUsed: boolean) => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "set",
      "proxyUsed",
      proxyUsed,
    ]);
  },
  setProxyName: (proxyName: string) => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "set",
      "proxyName",
      proxyName,
    ]);
  },
  setEntryHtmlPath: (entryHtmlPath: string) => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "set",
      "entryHtmlPath",
      entryHtmlPath,
    ]);
  },
  setWorkDir: (workDir: string) => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "set",
      "workDir",
      workDir,
    ]);
  },
  getUrl: (): Promise<unknown> => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "get",
      "url",
    ]);
  },
  getUserName: (): Promise<unknown> => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "get",
      "userName",
    ]);
  },
  getPassword: (): Promise<unknown> => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "get",
      "password",
    ]);
  },
  getProxyUsed: (): Promise<unknown> => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "get",
      "proxyUsed",
    ]);
  },
  getProxyName: (): Promise<unknown> => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "get",
      "proxyName",
    ]);
  },
  getEntryHtmlPath: (): Promise<unknown> => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "get",
      "entryHtmlPath",
    ]);
  },
  getWorkDir: (): Promise<unknown> => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "get",
      "workDir",
    ]);
  },
  showContents: () => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "action",
      "showContents",
    ]);
  },
  showOpenDialogForDirectory: () => {
    return window.electron.ipcRenderer.invoke(CONTENTS_VIEWER_CHANNEL, [
      "action",
      "showOpenDialogForDirectory",
    ]);
  },
};

export default ContentsViwerRenderer;
