import React from "react";
import { Box } from "@mui/material";
import { Outlet } from "react-router-dom";
import { Container } from "@mui/system";
import Sidebar from "./Sidebar";

const AppLayout = () => {
  return (
    <div>
      <Box sx={{ display: "flex" }}>
        <Sidebar />
        <Container component="main">
          <Box
            sx={{
              marginTop: 3,
              display: "flex",
              alignItems: "stretch",
              flexDirection: "column",
            }}
          >
            <Outlet />
          </Box>
        </Container>
      </Box>
    </div>
  );
};

export default AppLayout;
