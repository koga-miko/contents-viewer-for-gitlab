# contents-viewer-for-gitlab

GitLab で管理される HTML コンテンツをブラウザ表示するためのアプリケーションです

## 使い方

- アプリ起動後、`Home` の `Gitlab URL` 欄に、gitlab で管理されるリポジトリ最上位の URL もしくはリポジトリツリーの URL（※）のどちらかを入力し、コンテンツ表示ボタンを押下することで、標準でインストールされているブラウザ上に該当コンテンツが表示されます。

  - ※リポジトリツリーを指定する場合の形式： `https://<リポジトリのパス>/-/tree/<ブランチ名>`

- コンテンツ内の最初に起動する HTML ファイルは、`Settings` で変更可能です。デフォルトでは、コンテンツ配下の`public/index.html` になります。

- コンテンツをチェックアウトするフォルダは、Setting 画面で変更可能です。デフォルトでは、`C:\Users\<ユーザー名>\AppData\Roaming\contents-viewer-for-gitlab\contentsWork` になります。

## 前提条件

- git コマンドがインストールされていて、かつコマンドへのパスが通っている必要があります

- 対象の Gitlab サーバーへのアクセス権限を持っている必要があります（初回アクセス時にブラウザ上で認証画面が表示されますので認証を通す必要があります）

## Contents viewer for Gitlab アプリ開発環境のダウンロード～アプリ起動、およびインストーラ作成までの手順

- 【1. 共通の手順】環境のダウンロード

  1. コマンドプロンプトを開いて環境をダウンロードするための任意のディレクトリへ移動
  2. `git clone git@gitlab.com:koga-miko/contents-viewer-for-gitlab.git`
  3. `cd contents-viewer-for-gitlab`
  4. `npm i`

- 【2-(a). アプリを起動する場合】

  1. `npm start`

- 【2-(b). インストーラを作成する場合】

  1. `npm run publish`

  - out\make\squirrel.windows\x64 配下にインストーラ `contents-viewer-for-gitlab-<Version> Setup.exe` が生成される
  - 上記生成された インストーラを 任意の 64bit Windodws PC 環境で実行することで、アプリケーションがインストールされます

## アプリ開発時にやったことメモ

アプリ開発時の環境構築手順、ソースの主な変更内容、当アプリを構成する技術要素、引用ページなどなど、以下に記載。

### 事前準備

- node.js をインストール (以降、npm コマンドが使用可能)

- コーディングを円滑に進めるため、Visual Studio Code をインストールし、以下のプラグインをインストール
  - `Auto Close Tag`
  - `Auto Rename Tag`
  - `Babel JavaScript`
  - `ES7+ React/Resux/React-Native snippets`
  - `HTML CSS Support`
  - `Material Icon Theme`
  - `Prettier`
    - `Prettier` については、インストール後 `Settings` で以下の設定を実施することで、ファイルセーブ時に自動的にフォーマッタが動くようになる
      1. `Editor：Format On Save` にチェックを入れる
      2. `Editor：Default formatter` で、`Prettier - Code formatter` を使用する

### アプリの雛形作成までにやったこと

引用ページを参考に、Electron forge のページの記述に基づき、WebPack,TypeScript,React を用いた Electron アプリの雛形を作成。

1. コマンド `npm init electron-app@latest . -- --template=webpack-typescript` を実行

2. `tsconfig.json` の`"compilerOptions"`の中に、 `"jsx": "react-jsx"`, `"strict":true`を追加

3. コマンド `npm i --save react react-dom` を実行

4. コマンド `npm i --save-dev @types/react @types/react-dom` を実行

5. アプリで使用するライブラリをまとめてインストール

```
npm i react-router-dom @mui/material @mui/icons-material @emotion/react @emotion/styled @mui/lab simple-git electron-store
```

### 上記の環境構築後、追加・変更したソースの説明

上記の環境構築後、機能実現のために追加・変更したソースの内容について、以下に記載。

| ソース名                        | 動作コンテキスト or 設定 | 追加/変更 | 主な内容                                                                                                  |
| ------------------------------- | ------------------------ | --------- | --------------------------------------------------------------------------------------------------------- |
| `tsconfig.json`                 | TypeScript 設定          | 変更      | 厳密に型チェックすべく"strict:true"を追加                                                                 |
| `src/index.ts`                  | Main コンテキスト        | 変更      | Main コンテキストのエントリ。ContentsViwerMain 呼び出し等を追加。                                         |
| `src/ContentsViewerMain.ts`     | Main コンテキスト        | 追加      | 画面で設定・実施された内容を実際に保存・実行（ブラウザ表示など）するための実装                            |
| `src/preload.ts`                | Renderer コンテキスト    | 変更      | Renderer コンテキストのロード前処理。Remderer - Main 間の IPC を実装。                                    |
| `src/index.html`                | Renderer コンテキスト    | 変更      | Renderer コンテキストの表示コンテンツのトップ。React 流に id=root の div 要素を配置するように変更         |
| `src/renderer.ts`               | Renderer コンテキスト    | 変更      | 表示コンテンツ読み込み完了時にロードされるスクリプト。root.tsx を呼び出すよう変更                         |
| `src/root.tsx`                  | Renderer コンテキスト    | 追加      | index.html の id="root"の div 要素に対して、React 流に React の仮想 DOM のルートを紐づけるように対応      |
| `src/App.tsx`                   | Renderer コンテキスト    | 追加      | AppLayout を背景に Home 画面、Setting 画面を切り替えて React 流にシングルページとして表示できるように実装 |
| `src/AppLayout.tsx`             | Renderer コンテキスト    | 追加      | 画面左側にサイドバー(Sidebar)を配置し画面右側にアプリメイン表示部分の上部余白を設定するところまで実装     |
| `src/Sidebar.tsx`               | Renderer コンテキスト    | 追加      | Home と Setting を切り替える左側のバーを実装                                                              |
| `src/Home.tsx`                  | Renderer コンテキスト    | 追加      | Home 画面（URL 入力欄と表示実行ボタン）を実装。実処理は ContentsViewer(Renderer,Main) にゆだねる          |
| `src/Setting.tsx`               | Renderer コンテキスト    | 追加      | Settings 画面の実装。実処理は ContentsViewer(Renderer,Main) にゆだねる                                    |
| `src/ContentsViewerRenderer.ts` | Renderer コンテキスト    | 追加      | 画面で設定・実施された内容を Main コンテキスト側へ通知するための実装                                      |

### 当アプリを構成する技術的要素

当アプリを構成する環境・フレームワーク・ツール・言語・ライブラリなどの技術的要素を以下に記載。

| 概要                | 種類               | 内容                                                                                                            |
| ------------------- | ------------------ | --------------------------------------------------------------------------------------------------------------- |
| `Node.js`           | 環境               | JavaScript 実行環境。フロントエンドとバックエンドの両方で JavaScript によるアプリ開発をサポートする。           |
| `Electron`          | フレームワーク     | node 環境と Chromium ブラウザを使用してデスクトップアプリケーションを構築するためのフレームワーク。             |
| `Electron Forge`    | パッケージ化ツール | Electon アプリ作成からアプリのインストーラ配布までを手助けしてくれるツール。                                    |
| `TypeScript`        | 言語               | JavaScript に厳密な型指定が追加された言語。コンパイル時点で問題検出可能になるケースが多く安全。                 |
| `React`             | ライブラリ         | メジャーな WebUI 構築用ライブラリの１つ。コンポーネント指向で再利用性・保守性が高い。高速。                     |
| `React Router DOM`  | ライブラリ         | React によるシングルページアプリ作成のため Web アプリに動的ルーティングを実装できるようにするためのライブラリ。 |
| `React Material UI` | ライブラリ         | Google の Material Design をベースに作られた React 用 UI コンポーネントライブラリ。                             |

### 引用ページ

当アプリを開発するにあたり参考にした主なページを以下に記載。

1. [typescript + webpack-template in Electron Forge](https://www.electronforge.io/templates/typescript-+-webpack-template)

2. [react-with-typescript in Electron Forge](https://www.electronforge.io/guides/framework-integration/react-with-typescript)

3. [create-a-new-react-app in React](https://reactjs.org/docs/create-a-new-react-app.html#create-react-app)

4. [IPC in Electron](https://www.electronjs.org/ja/docs/latest/tutorial/ipc)

5. [Material UI](https://mui.com/)

6. [The examples for react-router-dom](https://www.techiediaries.com/react/react-router-5-4-tutorial-examples)

7. [Simple-Git](https://www.npmjs.com/package/simple-git)

8. [Electron Store](https://www.npmjs.com/package/electron-store)
